Esta aplicación permite crear, modificar, visualizar y eliminar clientes con información básica de su tarjeta de crédito.

Se trata de una aplicación hecha en HTML5, CSS3 y JavaScript (handlebars) que permite hacer un CRUD a un API REST creada en Node.js, además, permite realizar el registro de usuarios encriptando contraseñas.

La base de datos usada es MySQL, en esta se almacena toda la información, tanto de los usuarios como de los clientes, permitiendo que los clientes sean privados y solo el usuario que los registró los pueda ver, editar o borrar.

Para poder iniciar la aplicación es necesario realizar los siguientes pasos:

1.	Tener instalado node en el equipo, se puede descargar desde la siguiente URL: https://nodejs.org/es/
2.	Tener instalado y configurado MySQL
3.	Crear una carpeta, abrir una consola de comandos en ese directorio y clonar el repositorio con el siguiente comando: "git clone https://gitlab.com/MonterrosaF4/hightech-fullstack"
4.	Dentro de la carpeta clonada (hightech-fullstack) abrir la carpeta database, en esta carpeta hay un archivo llamado db.sql, copiar todo el código y ejecutarlo con MySQL para preparar la base de datos. (Se maneja la misma base de datos del proyecto de Angular, si la db ya está creada, omitir este paso)
5.	Dentro de la carpeta clonada (hightech-fullstack) abrir el archivo .env
6.	En este archivo debemos escribir la información para realizar la conexion a nuestra base de datos (host, usuario, contraseña y base de datos), por ejemplo:

HOST_DB=localhost

USER_DB=root

PASSWORD_DB=toor

DATABASE_DB=database_hightech

NOTA: guardar el archivo ".env"

7.	Si la contraseña de MySQL es de baja complejidad, automáticamente el servidor lanzará un error de seguridad, para corregir este inconveniente es necesario modificar los permisos del usuario con el que se está iniciando sesión con el siguiente comando en SQL: alter user 'user@host' identified with mysql_native_password by 'password' . Se deben modificar los campos de user, host y password que se encuentran dentro de las comillas simples, para mayor información revisar el siguiente link: https://stackoverflow.com/questions/50373427/node-js-cant-authenticate-to-mysql-8-0
8.  Ingresar a la carpeta clonada (hightech-fullstack) por medio de la consola e instalar todas las dependencias necesarias con el comando "npm install"
8.	Ingresar en la carpeta backend por medio de la consola y ejecutar el comando "npm run dev"
9.	Cuando este comando finalice su ejecución se lanzará un mensaje por consola con "Servidor corriendo en el puerto 4000", podremos entrar en la URL http://localhost:4000/