const mysql = require("mysql");
const { promisify } = require("util");
const { database } = require("./keys");
const { Console } = require("console");

const pool = mysql.createPool(database);

pool.getConnection((err, connection) => {
  if (err) {
    if (err.code === "PROTOCOL_CONNECTION_LOST") {
      console.error("CONEXION CERRADA CON LA BASE DE DATOS");
    }
    if (err.code === "ER_ACCESS_DENIED_ERROR") {
      console.error("CONTRASEÑA ERRONEA EN LA BD O DATOS INCORRECTOS");
    }
    if (err.code === "ER_CON_COUNT_ERROR") {
      console.error("LA BASE DE DATOS TIENE MUCHAS CONEXIONES");
    }
    if (err.code === "ECONNREFUSED") {
      console.error("LA CONEXION FUE RECHAZADA");
    }
  }
  if (connection) connection.release();
  // console.log("CONEXION EXITOSA A LA BASE DE DATOS");
  return;
});

pool.query = promisify(pool.query);

module.exports = pool;
