// Instancias
const express = require("express");
const morgan = require("morgan");
const exphbs = require("express-handlebars");
const path = require("path");
const flash = require("connect-flash");
const session = require("express-session");
const MySqlStore = require("express-mysql-session");
const passport = require("passport");

const { database } = require("./keys");

// Inicializaciones
const app = express();
require("./lib/passport");

// Configuraciones
app.set("port", process.env.PORT || 4000);
app.set("views", path.join(__dirname, "views"));
app.engine(
  ".hbs",
  exphbs({
    defaultLayout: "main",
    layoutsDir: path.join(app.get("views"), "layouts"),
    partialsDir: path.join(app.get("views"), "partials"),
    extname: ".hbs",
  })
);
app.set("view engine", ".hbs");

// Middlewares
app.use(
  session({
    secret: "hightechsession",
    resave: false,
    saveUninitialized: false,
    store: new MySqlStore(database),
  })
);
app.use(flash());
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(passport.initialize());
app.use(passport.session());

// Variables globales
app.use((req, res, next) => {
  app.locals.success = req.flash("success");
  app.locals.message = req.flash("message");
  app.locals.errorm = req.flash("errorm");
  app.locals.user = req.user;
  next();
});

// Rutas
app.use(require("./routes/index"));
app.use(require("./routes/authentication"));
app.use("/clients", require("./routes/clients"));

// Publics
app.use(express.static(path.join(__dirname, "public")));

// Inicializando servidor
app.listen(app.get("port"), () => {
  console.log("Servidor corriendo en el puerto", app.get("port"));
});
