const express = require("express");
const router = express.Router();

const pool = require("../database");
const { isLoggedIn } = require("./../lib/auth");

router.get("/add", isLoggedIn, (req, res) => {
  res.render("clients/add");
});

router.post("/add", isLoggedIn, async (req, res) => {
  const {
    cid,
    first_name,
    last_name,
    tc_number,
    cvv,
    expiration_date,
  } = req.body;
  const newClient = {
    cid,
    first_name,
    last_name,
    tc_number,
    cvv,
    user_id: req.user.id,
    expiration_date,
  };
  await pool.query("INSERT INTO clients set ?", [newClient]);
  req.flash("success", "Cliente agregado correctamente");
  res.redirect("/clients");
});

router.get("/", isLoggedIn, async (req, res) => {
  const clients = await pool.query("SELECT * FROM clients WHERE user_id = ?", [
    req.user.id,
  ]);
  res.render("clients/list", { clients });
});

router.get("/delete/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  await pool.query("DELETE FROM clients WHERE ID = ?", [id]);
  req.flash("success", "Cliente eliminado correctamente");
  res.redirect("/clients");
});

router.get("/edit/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const client = await pool.query("SELECT * from clients WHERE ID = ?", [id]);
  res.render("clients/edit", { client: client[0] });
});

router.post("/edit/:id", isLoggedIn, async (req, res) => {
  const { id } = req.params;
  const {
    cid,
    first_name,
    last_name,
    tc_number,
    cvv,
    expiration_date,
  } = req.body;
  const newClient = {
    cid,
    first_name,
    last_name,
    tc_number,
    cvv,
    expiration_date,
  };
  await pool.query("UPDATE clients set ? WHERE ID = ?", [newClient, id]);
  req.flash("success", "Cliente actualizado correctamente");
  res.redirect("/clients");
});

module.exports = router;
